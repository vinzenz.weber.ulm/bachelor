﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;


public class HoldPad : MonoBehaviour
{
    public SteamVR_Behaviour_Pose RightPose = null;
    //private SteamVR_TrackedController _controller;
    public SteamVR_Action_Boolean MenuButton = null;

    //public SteamVR_Skeleton_Poser Pose = null;
    public GameObject Pad;
    public GameObject RightHand;

    [HideInInspector]
    public bool Active;



    // Start is called before the first frame update
    void Start()
    {

        Pad.SetActive(false);
        Active = false;
        //Pose = Pad.GetComponent<SteamVR_Skeleton_Poser>();
    }

    // Update is called once per frame
    void Update()
    {
        if (MenuButton.GetStateDown(RightPose.inputSource) && Active)
        {
            HidePad();
            return;
        }

        if (MenuButton.GetStateDown(RightPose.inputSource) && !Active)
        {
            ShowPad();
            return;
        }
    }


    private void ShowPad()
    {
        //print("Hallo");
        //Pose.GetBlendedPose();
        Pad.SetActive(true);
        //Pose.GetBlendedPose(RightPose);
        Active = true;
        //Pose.GetBlendingBehaviour("Holding");
        //print(MenuButton.GetStateDown(RightPose.inputSource));
        //RightHand.GetComponent<SkinnedMeshRenderer>().enabled = false;

    }
    private void HidePad()
    {
        //print("Du");
        //Pose.GetBlendedPose();
        Pad.SetActive(false);
        //Pose.GetBlendedPose(RightPose);
        Active = false;
        //RightHand.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;

    }
}
