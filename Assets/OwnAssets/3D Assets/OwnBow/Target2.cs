﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Target2 : MonoBehaviour, IDamagable
{
    public static Target2 instance;

    private Animator animator = null;
    public GameObject Text = null;

    [HideInInspector]
    public bool hitted = false;
    public GameObject ParticleEffect;



    //private bool targethit1;
    //private bool targethit2;
    //private bool targethit3;

    //public Arrow ArrowScript;

    //private String Tag;

    private int count;




    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        animator = Text.GetComponent<Animator>();
    }

    public void Damage(int amount)
    {
        StartAnim();
        //GetTag();
        SetTrue();
    }



    private void StartAnim()
    {
        animator.SetBool("NICE", true);
        Invoke("StopAnim", 0.25f);
    }

    void StopAnim()
    {
        animator.SetBool("NICE", false);
    }

    public void SetTrue()
    {
        hitted = true;
        AllTargetsHit();
    }

    public void AllTargetsHit()
    {
        if(hitted && TargetNice.instance.hitted && Target3.instance.hitted)
        {
            if(count <1)
            {
                print("Getroffen");
                ParticleEffect.SetActive(true);
                count = 1;

            }
        }
    }

    //private void GetTag()
    //{
    //    Tag = Arrow.instance.HittedObject.tag;

    //    print(Arrow.instance.HittedObject.tag);

    //    if(Tag == "Target1")
    //    {
    //        targethit1 = true;
    //    }
    //    if (Tag == "Target2")
    //    {
    //        targethit2 = true;
    //    }
    //    if (Tag == "Target3")
    //    {
    //        targethit3 = true;
    //    }

    //    if(targethit1 && targethit2 && targethit3)
    //    {
    //        AllTargetsHit();
    //    }
    //}

    //private void AllTargetsHit()
    //{
    //    Debug.Log("<color=red>Status: </color>Alle targets gehitted");

    //}
}
