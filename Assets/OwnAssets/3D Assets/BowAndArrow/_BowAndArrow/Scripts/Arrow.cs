﻿using UnityEngine;
using System;

public class Arrow : MonoBehaviour
{
    public static Arrow instance;

    public float m_Speed = 2000.0f;
    public Transform m_Tip = null;

    private Rigidbody m_Rigidbody = null;
    private bool m_IsStopped = true;
    private Vector3 m_LastPosition = Vector3.zero;

    private int layerMask = 1 << 12; //So the level barriers get excluded

    public GameObject TracerObj = null;
    private Light Tracer = null;

    [HideInInspector]
    public GameObject HittedObject;

    [HideInInspector]
    public String Tag = null;





    public void Awake()
    {
        instance = this;
        layerMask = ~layerMask;
        m_Rigidbody = GetComponent<Rigidbody>();
        Tracer = TracerObj.GetComponent<Light>();
        TracerOff();
    }

    private void Start()
    {
        m_LastPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (m_IsStopped)
            return;

        //Rotate in Direction of Flight


        m_Rigidbody.MoveRotation(Quaternion.LookRotation(m_Rigidbody.velocity, transform.up));

        //CollisionCheck
        RaycastHit hit;
        if (Physics.Linecast(m_LastPosition, m_Tip.position, out hit, layerMask ))
        {
              Stop(hit.collider.gameObject);
            

        }

        //Store Position
        m_LastPosition = m_Tip.position;
    }


    private void Stop(GameObject hitObject)
    {
        m_IsStopped = true;

        //Parent
        transform.parent = hitObject.transform;
        

        m_Rigidbody.isKinematic = true;
        m_Rigidbody.useGravity = false;

        //Damage
        CheckForDamage(hitObject);

        //Turn the Tracer off
        Invoke("TracerOff", 5);

        

        
    }

    public void Fire(float pullValue)
    {
        m_LastPosition = transform.position;


        m_IsStopped = false;
        transform.parent = null;

        m_Rigidbody.isKinematic = false;
        m_Rigidbody.useGravity = true;
        m_Rigidbody.AddForce(transform.forward * (pullValue * m_Speed));

        TracerOn();

        Tracer.enabled = true;

        Destroy(gameObject, 30.0f);
    }

    private void CheckForDamage(GameObject hitObject)
    {
        MonoBehaviour[] behaviours = hitObject.GetComponents<MonoBehaviour>();

        foreach(MonoBehaviour behaviour in behaviours)
        {
            if(behaviour is IDamagable)
            {
                IDamagable damageable = (IDamagable)behaviour;
                damageable.Damage(5);

                //Save the hitted Object
                HittedObject = hitObject;

                //Get Tag
                Tag = hitObject.tag;

                break;
            }
        }
    }

    public void TracerOff()
    {
        Tracer.enabled = false;
    }
    public void TracerOn()
    {
        Tracer.enabled = true;
    }
}
