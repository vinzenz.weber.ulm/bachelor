﻿using UnityEngine;
using System.Collections;

using Valve.VR;

public class SteamInput : MonoBehaviour
{
    
    public Bow m_Bow = null;
    public Rigidbody m_Rigidbody = null;
    public SteamVR_Behaviour_Pose m_Pose = null;
    public SteamVR_Action_Boolean m_PullAction = null;
    public SteamVR_Action_Boolean m_GrabAction = null;

    [Header("Hands")]
    public SteamVR_Behaviour_Pose LeftPose = null;
    public SteamVR_Behaviour_Pose RightPose = null;
    public Transform LeftHand;
    public Transform RightHand;

    public float threshold = 1;

    private bool attachedToRight = false;
    private bool attachedToLeft = false;

    private bool m_IsStopped;

    private Vector3 m_LastPosition;

    [Header("Colloder Logic")]
    public GameObject ArrowObj = null;
    //public GameObject StringObj = null;
    public Collider ArrowCol = null;
    //public Collider StringCol = null;

    private int count = 0;


    public void Awake()
    {
        ArrowCol = ArrowObj.GetComponent<Collider>();
        //StringCol = StringObj.GetComponent<Collider>();
    }



    private void Update()
    {
        //Get distance from Hand To Bow
        float distanceLeft = Vector3.Distance(LeftHand.position, m_Bow.transform.position);
        float distanceRight = Vector3.Distance(RightHand.position, m_Bow.transform.position);

        


        //Attach Bow to Hand
        

        if (distanceLeft < threshold && m_GrabAction.GetStateDown(LeftPose.inputSource) && !attachedToRight && !attachedToLeft)
        {
            
            AttachToHand(LeftHand);
        }

        if (distanceRight < threshold && m_GrabAction.GetStateDown(RightPose.inputSource) && !attachedToRight && !attachedToLeft)
        {
      
            AttachToHand(RightHand);
        }

        if (m_GrabAction.GetStateUp(RightPose.inputSource) && attachedToRight)
        {
            ReleaseTheBow();
        }

        if (m_GrabAction.GetStateUp(LeftPose.inputSource) && attachedToLeft)
        {
            ReleaseTheBow();
        }
        

        //if (Physics.Linecast(m_LastPosition, m_Bow.transform.position))
        //{
        //    Stop();
        //}

        m_LastPosition = m_Bow.transform.position;



        //Change Pull Hand according to Attached Hand


        if (m_PullAction.GetStateDown(m_Pose.inputSource))
            m_Bow.Pull(m_Pose.gameObject.transform);

        if (m_PullAction.GetStateUp(m_Pose.inputSource))
            m_Bow.Release();
    }


    private void AttachToHand(Transform Hand)
    {
        TurnOffCollider();
       
        m_Bow.transform.SetParent(Hand);
        
        //Transform position

        m_Bow.transform.localPosition = new Vector3(0, 0.02f,0);
        m_Bow.transform.localEulerAngles = new Vector3(90, 0, 0);

        


        if (Hand == LeftHand)
        {
            attachedToLeft = true;
            m_Pose = RightPose;
        }
        if (Hand == RightHand)
        {
            attachedToRight = true;
            m_Pose = LeftPose;
        }



    }

    private void ReleaseTheBow()
    {
        m_Bow.transform.SetParent(null);
        TurnOnCollider();
        attachedToRight = false;
        attachedToLeft = false;
        


        //m_IsStopped = false;

        //m_Rigidbody.isKinematic = false;
        //m_Rigidbody.useGravity = true;

    }

    //private void Stop()
    //{
    //    m_IsStopped = true;

    //    m_Rigidbody.isKinematic = true;
    //    m_Rigidbody.useGravity = false;
    //}


    public void TurnOffCollider()
    {

        m_Rigidbody.isKinematic = true;
        m_Rigidbody.useGravity = false;
        ArrowCol.enabled = false;
        //StringCol.enabled = false;
    }
 
    private IEnumerator WaitForCollider(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);
        if(!attachedToLeft && !attachedToRight)
        {
            ArrowCol.enabled = true;
        }
        //StringCol.enabled = true;

    }



    public void TurnOnCollider()
    {

        m_Rigidbody.isKinematic = false;
        m_Rigidbody.useGravity = true;

        StartCoroutine(WaitForCollider(0.2f));
        //m_Rigidbody.AddForce(RightHand.transform.forward * 0.5f);
        
    }
}
