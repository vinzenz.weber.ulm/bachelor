﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPLayerScript : MonoBehaviour
{

    public VideoPlayer videoPlayer;
    public GameObject VideoObject;
    // Start is called before the first frame update
    void Start()
    {
        videoPlayer = VideoObject.GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayVideo()
    {
        videoPlayer.Play();
        videoPlayer.isLooping = false;
    }
}
