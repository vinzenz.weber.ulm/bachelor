﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerUI : MonoBehaviour
{


    public RawImage Image;

    public VideoPlayer Video;
    // Start is called before the first frame update
    void Start()
    {
        Image.enabled = false;
        StartCoroutine(StartVideo());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator StartVideo()
    {
        Video.Prepare();
        WaitForSeconds waitforseconds = new WaitForSeconds(1);
        while(Video.isPrepared)
        {
            yield return waitforseconds;
            break;
        }

        Image.texture = Video.texture;
        Video.Play();
        Image.enabled = true;
    }
}
