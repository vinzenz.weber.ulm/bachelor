﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenSpacemanager : MonoBehaviour
{
    public Transform target;
    public GameObject CamObject;
    public Camera cam;
    public Image Pointer;
    public GameObject Panel;
    public Canvas canvas;
    public RectTransform CanvasRect;

    void Start()
    {
        cam = CamObject.GetComponent<Camera>();
        CanvasRect = canvas.GetComponent<RectTransform>();
    }

    void Update()
    {
        Vector3 screenPos = cam.WorldToScreenPoint(target.localPosition);
        Debug.Log("target is " + screenPos.x + " pixels from the left");
        Debug.Log("target is " + screenPos.y + " pixels from the top");

        Pointer.transform.localPosition =  new Vector2((screenPos.x*CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f), (screenPos.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f));

        
    }
}