﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CapsuleLogic : MonoBehaviour
{

    [Header("Debug")]
    public bool Debug;

    [Header("Timing")]
    [Tooltip("Für Timing von Lande Animationen und Trigger")]
    public int LandTime = 15;

    [Tooltip("Für Timing von Animationen und Trigger nachdem Türe geöffnet")]
    public int DoorOpenTime = 1;

    [Header("Door")]
    public GameObject DoorObject;
    public Animator DoorAnim;

    [Header("Capsule")]
    public GameObject CapsuleObject;
    public Animator CapsuleAnim;

    [Header("Player")]
    public GameObject PlayerObject;
    //public Animator PlayerAnim;

    public GameObject Hangar;
    public GameObject Terrain;

    [Header("Particles")]
    public GameObject LandShockWave;   //Rauchbildung bei Landung         

    [Header("Sounds")]
    public GameObject ForestSound;
    public AudioSource ForestSoundAudio;

    public GameObject UHUSound;
    public AudioSource UHUSoundAudio;

    [Header("Distance Threshold Player Capsule")]
    [Tooltip("Ab Dieser Distanz wird die Restart animation der Kapsel Abgespielt")]
    public float DistFromCapsule;


    [Header("LandePunkt")]
    public GameObject LandingPoint;

    public bool Landed;

    






    // Start is calledbefore the first frame update
    void Start()
    {
        if (!Debug)
        {
            
            PlayerObject.transform.SetParent(CapsuleObject.transform);
            PlayerObject.transform.localPosition = new Vector3(0, 0, 0);  //localPosition, sonst ist PlayerObject IRGEDNWO außer (0, 0, 0) lülz
            print(PlayerObject.transform.position);
        }
        if(Debug)
        {
            RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;
            Terrain.SetActive(true);
            Hangar.SetActive(false);
        }

        //Animationen
        DoorAnim = DoorObject.GetComponent<Animator>();
        CapsuleAnim = CapsuleObject.GetComponent<Animator>();

        //Audio
        ForestSoundAudio = ForestSound.GetComponent<AudioSource>();
        UHUSoundAudio = UHUSound.GetComponent<AudioSource>();


        Landed = false;
    }



    // Update is called once per frame

    
void Update()
{
    float dist = Vector3.Distance(PlayerObject.transform.position, LandingPoint.transform.position);
    print(dist);
    if(dist > DistFromCapsule && Landed)
    {
        CapsuleAnim.SetBool("shouldHover", true);
            DoorAnim.SetBool("open", false);
    }
}



    public void StartCapsule()   //Animation für kapselFlug wird gestartet
    {
        CapsuleAnim.SetBool("start", true);
        //PlayerAnim.SetBool("start", true);
    }

    public void CloseDoor()   //Türe wird Geschlossen
    {

        DoorAnim.SetBool("open", false);

        StartCoroutine(SetTrue()); //Sonst geht türe immer wieder auf und zu du weißt

        StartCoroutine(TimeUntilLanded());
        StartCoroutine(TimeUntilDoorOpened());


    }

    IEnumerator SetTrue()
    {
        yield return new WaitForSeconds(2);

        DoorAnim.SetBool("open", true);   

        yield return new WaitForSeconds(2);

        Hangar.SetActive(false);
        Terrain.SetActive(true);

        RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;  // Skybox wird zu Gradient

        yield return new WaitForSeconds(LandTime + DoorOpenTime*2);
        PlayerObject.transform.SetParent(null);    // Player ist nicht mehr child von Kapsel und kann  sich somit wieder frei bewegen
    }



    IEnumerator TimeUntilLanded()
    {
        yield return new WaitForSeconds(LandTime-2);

        CapsuleLanded();
    }

    IEnumerator TimeUntilDoorOpened()
    {
        yield return new WaitForSeconds(LandTime + DoorOpenTime*2);

        ReadyToLeaveCapsule();
    }

    public void CapsuleLanded()
    {
        LandShockWave.SetActive(true);
      
        Landed = true;
    }

    public void ReadyToLeaveCapsule()
    {
        ForestSoundAudio.Play();
        UHUSoundAudio.Play();
    }

}
