﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Valve.VR.InteractionSystem
{
    public class Compass : MonoBehaviour
    {

        public Vector3 NorthDirection;
        public Transform Player;
        public Quaternion [] ArtefactDirections;


        public GameObject Kompasse;
        public GameObject Pad;


  



        public GameObject[] ArtefactPointers;

        public Transform[] Artefacts;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            ChangeArtefactDirection();
           MoveWithPad();
        }




        public void ChangeArtefactDirection()
        {
            for (int i = 0; i < Artefacts.Length; i++)
            {
                Vector3 dir = transform.position - Artefacts[i].position;

                ArtefactDirections[i] = Quaternion.LookRotation(dir);

                ArtefactPointers[i].transform.localRotation = ArtefactDirections[i] * Quaternion.Euler(NorthDirection);
            }

            //if (HUDManager.instance.artefact1found)
            //{
            //    ArtefactPointers[0].SetActive(false);
            //}
            if (HUDManager.instance.artefact2found)
            {
                ArtefactPointers[0].SetActive(false);
            }
            if (HUDManager.instance.artefact3found)
            {
                ArtefactPointers[1].SetActive(false);
            }
            if (HUDManager.instance.artefact4found)
            {
                ArtefactPointers[2].SetActive(false);
            }

        }

        public void MoveWithPad()
        {
            Kompasse.transform.localPosition = new Vector3(Pad.transform.position.x, Pad.transform.position.y, Pad.transform.position.z);
        }
    }
}

