﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Valve.VR.InteractionSystem
{
    public class HUDManager: MonoBehaviour
    {
        public static HUDManager instance;

        [HideInInspector]
        public int artefactTag;

        [Header("GameObjects")]
        public Toggle Slot1;
        public Toggle Slot2;
        public Toggle Slot3;
        public GameObject Canvas;

        //[Header("Start Button Hilfe UI")]
        //public GameObject HelpCanvas;

        [Header("Meitingen Canvas")]
        public GameObject MeitingenCanvas;

        public bool artefact1found;
        public bool artefact2found;
        public bool artefact3found;
        public bool artefact4found;
        public bool artefact5found;



        void Awake()
        {
            instance = this;
        }

        void Start()
        {
            //Fasle = unchecked

            Slot1.isOn = false;
            Slot2.isOn = false;
            Slot3.isOn = false;
            //Canvas.SetActive(false);

            //HelpCanvas.SetActive(false);

        


        //StartCoroutine(_ActivateHelpCanvas());



    }
        void Update()
        {
            //print(artefactTag);

            //if (artefactTag == 1)
            //{
            //    Slot1.isOn = true;
            //    artefact1found = true;

            //}
            if (artefactTag == 2)
            {
                Slot2.isOn = true;
                artefact2found = true;
            }
            if (artefactTag == 3)
            {
                Slot3.isOn = true;
                artefact3found = true;
            }
            if(artefactTag == 4)
            {
                artefact4found = true;
            }
            //if (artefactTag == 5)
            //{
            //    artefact5found = true;
            //}

        }

        public void PrintTest()
        {
            print(artefactTag);
        }

        public void ToggleCanvas()
        {
            if(!MainLogic.instance.CatchedEmAll)
            {
                StartCoroutine(_ToggleCanvas());
            }
            
        }

        public IEnumerator _ToggleCanvas()
        {
            yield return new WaitForSeconds(2);

            //Canvas.SetActive(true);
        }


        

        public IEnumerator _ActivateHelpCanvas()
        {
            yield return new WaitForSeconds(2);

            //HelpCanvas.SetActive(true);
        }

        public void _DeactivateHelpCanvas()
        {
            if (!MainLogic.instance.CatchedEmAll)
            {
                //HelpCanvas.SetActive(false);
            }
        }

        //public void DeactivateMeitingenCanvas()
        //{
        //    StartCoroutine(_DeactivateMeitingenCanvas());
        //}

        //public IEnumerator _DeactivateMeitingenCanvas()
        //{
        //    yield return new WaitForSeconds(12);


        //    MeitingenCanvas.GetComponent<CanvasGroup>().alpha =  Mathf.Lerp(1, 0, 0.01f);

        //    //MeitingenCanvas.SetActive(false);
        //}
    }
}