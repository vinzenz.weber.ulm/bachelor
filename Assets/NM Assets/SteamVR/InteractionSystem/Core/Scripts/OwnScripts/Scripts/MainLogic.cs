﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.InteractionSystem;




namespace Valve.VR.InteractionSystem
{
    public class MainLogic : MonoBehaviour
    {
        public static MainLogic instance;

        [Header("Debug")]
        public bool Debug;
        public bool ScreenShots;
        public GameObject SunScreenCapture;
        public GameObject Sun;
        public GameObject DebugSun;
        public GameObject Begrenzungen;

        [Header("Timing")]
        [Tooltip("Für Timing von Lande Animationen und Trigger")]
        public int LandTime = 15;

        [Tooltip("Für Timing von Animationen und Trigger nachdem Türe geöffnet")]
        public int DoorOpenTime = 1;

        [Header("Door")]
        public GameObject DoorObject;
        public Animator DoorAnim;

        [Header("Capsule")]
        public GameObject CapsuleObject;
        public Animator CapsuleAnim;
        public GameObject PlayerSpawn;

        [Header("Player")]
        public GameObject PlayerObject;
        //public Animator PlayerAnim;

        public GameObject Hangar;
        public GameObject Terrain;

        [Header("Particles")]
        public GameObject LandShockWave;   //Rauchbildung bei Landung         

        [Header("Sounds")]
        public GameObject ForestSound;
        public AudioSource ForestSoundAudio;

        public GameObject UHUSound;
        public AudioSource UHUSoundAudio;

        //[Header("Props")]
        //public GameObject Flashlight;

        //public GameObject AlarmSound;
        //public AudioSource AlarmSoundAudio;

        [Header("Distance Threshold Player Capsule")]
        [Tooltip("Ab Dieser Distanz wird die Restart animation der Kapsel Abgespielt")]
        public float DistFromCapsule;


        [Header("LandePunkt")]
        public GameObject LandingPoint;

        [Header("TeleportArea")]
        public GameObject TeleportArea;

        [Header("Teleporting Object")]
        public GameObject Teleporting;
        public Teleport _teleport;

        public bool Landed;

        public HUDManager _HudManager;

        [HideInInspector]
        public bool CatchedEmAll;

        //[Header("HelpCanvas")]
        public GameObject Panel;
        public Animator PanelAnim;
        private bool ausgelöst = false;
        private float dist2;
        private int threshold2 = 15;
        

        [Header("Skyboxen")]
        public Material Labor;
        public Cubemap LaborReflection;
        public Material Himmel;
        public Cubemap HimmelReflection;


        [Header("Kompasse")]
        public GameObject HelpDots;
        public GameObject CompassCapsule;
        public GameObject Kompasse;

        [Header("Bogen")]
        public Transform Bogen = null;

        [Header("iPad Logic")]
        public SteamVR_Behaviour_Pose RightPose = null;
        public SteamVR_Action_Boolean MenuButton = null;

        //public SteamVR_Skeleton_Poser Pose = null;
        public GameObject Pad;
        public GameObject RightHand;

        private bool PadActive;
        private int PadOpenCounter = 0;

        public Hand RechteHand;
        public SteamVR_Action_Boolean MenuAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("Menu");

        [HideInInspector]
        public bool Active;

        [Header("UI Logic")]
        public Toggle Topf;
        public Toggle Pflug;
        public Toggle Knochen;
        public Toggle Cube;
        public Toggle Cube2;
        private Animator PadAnim;

        public GameObject ItemScreen;
        public GameObject ReturnScreen;

        [Header("Hologramms")]
        public GameObject TopfHolo;
        public GameObject PflugHolo;
        public GameObject KnochenHolo;
        //public GameObject CubeHolo;
        //public GameObject Cube1Holo;
        public GameObject Holo;













        void Awake()
        {

            instance = this;
            //PlayerObject.transform.position = new Vector3(Positioner.transform.position.x, Positioner.transform.position.y, Positioner.transform.position.z);
            //Positioner.transform.position = new Vector3(PlayerObject.transform.position.x, PlayerObject.transform.position.y, PlayerObject.transform.position.z);
        }


        // Start is calledbefore the first frame update
        void Start()
        {

            Pad.SetActive(false);
            Active = false;
            CatchedEmAll = false;

            if (!Debug && !ScreenShots)
            {

                HelpDots.SetActive(false);
                RenderSettings.skybox = Labor;
                RenderSettings.customReflection = LaborReflection;
                //Panel.SetActive(false);
                //FindArtefacts.SetActive(false);
                //ComeBack.SetActive(false);
                PlayerObject.transform.SetParent(PlayerSpawn.transform);
                PlayerObject.transform.localPosition = new Vector3(0, 0, 0);  //localPosition, sonst ist PlayerObject IRGEDNWO außer (0, 0, 0) lülz
                print(PlayerObject.transform.position);
                Terrain.SetActive(false);
                Hangar.SetActive(true);
                DebugSun.SetActive(false);
                Sun.SetActive(true);
                Begrenzungen.SetActive(true);
                //Flashlight.SetActive(false);
                TeleportArea.SetActive(false);
                Teleporting.SetActive(false);
                CompassCapsule.GetComponent<Renderer>().enabled = false;
            }

            if (ScreenShots)
            {

                PlayerObject.SetActive(false);
                HelpDots.SetActive(false);
                RenderSettings.skybox = Labor;
                RenderSettings.customReflection = LaborReflection;
                //Panel.SetActive(false);
                //FindArtefacts.SetActive(false);
                //ComeBack.SetActive(false);
                //PlayerObject.transform.SetParent(PlayerSpawn.transform);
                //PlayerObject.transform.localPosition = new Vector3(0, 0, 0);  //localPosition, sonst ist PlayerObject IRGEDNWO außer (0, 0, 0) lülz
                //print(PlayerObject.transform.position);
                //Terrain.SetActive(true);
                Hangar.SetActive(true);
                DebugSun.SetActive(false);
                Sun.SetActive(false);
                SunScreenCapture.SetActive(true);
                Begrenzungen.SetActive(true);
                //Flashlight.SetActive(false);
                TeleportArea.SetActive(false);
                Teleporting.SetActive(false);
                CompassCapsule.GetComponent<Renderer>().enabled = false;
            }

            if (Debug && !ScreenShots)
            {
                //RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;
                Terrain.SetActive(true);
                Hangar.SetActive(false);
                DebugSun.SetActive(true);
                Sun.SetActive(false);
                Begrenzungen.SetActive(false);
                //Flashlight.SetActive(true);
                Teleporting.SetActive(true);
                HelpDots.SetActive(true);
                CompassCapsule.GetComponent<Renderer>().enabled = true;

            }

            //Animationen
            DoorAnim = DoorObject.GetComponent<Animator>();
            CapsuleAnim = CapsuleObject.GetComponent<Animator>();
            PanelAnim = Panel.GetComponent<Animator>();
            PadAnim = Pad.GetComponent<Animator>();

            //Audio
            ForestSoundAudio = ForestSound.GetComponent<AudioSource>();
            UHUSoundAudio = UHUSound.GetComponent<AudioSource>();
            //AlarmSoundAudio = AlarmSound.GetComponent<AudioSource>();
            


            Landed = false;
        }



        // Update is called once per frame


        void Update()
        {
            

            if (Bogen.position.y <= 22)
            {
                Bogen.position = new Vector3(Bogen.position.x, 28, Bogen.position.z);
            }

            //Scene Neu Laden
            if (Input.GetKeyDown(KeyCode.R))
            {
                RestartGame();
            }

            float dist = Vector3.Distance(PlayerObject.transform.position, LandingPoint.transform.position);
            //print(dist);
            //if (dist > DistFromCapsule && Landed)
            //{
            //    CapsuleAnim.SetBool("shouldHover", true);
            //    //DoorAnim.SetBool("open", false);
            //}


            if(_HudManager.artefact2found && _HudManager.artefact3found && _HudManager.artefact4found && Landed)
            {
                FoundAllItems();
            }
            //if(_HudManager.artefact1found)
            //{
            //    Cube.isOn = true;
            //    Holo.SetActive(true);
            //    //ShowMenuHint(RechteHand);
            //}
            if (_HudManager.artefact2found)
            {
                Topf.isOn = true;
                Holo.SetActive(true);
                PflugHolo.SetActive(false);
                KnochenHolo.SetActive(false);
                Invoke("TopfHoloOn", 3);

                //ShowMenuHint(RechteHand);
            }
            if (_HudManager.artefact3found)
            {
                Knochen.isOn = true;
                Holo.SetActive(true);
                TopfHolo.SetActive(false);
                PflugHolo.SetActive(false);
                Invoke("KnochenHoloOn", 3);

                //ShowMenuHint(RechteHand);
            }
            if (_HudManager.artefact4found)
            {
                Pflug.isOn = true;
                Holo.SetActive(true);
                TopfHolo.SetActive(false);
                KnochenHolo.SetActive(false);
                Invoke("PflugfHoloOn", 3);

                //ShowMenuHint(RechteHand);
            }
            //if (_HudManager.artefact5found)
            //{
            //    Cube2.isOn = true;
            //    Holo.SetActive(true);

            //    //ShowMenuHint(RechteHand);
            //}


            if (!ausgelöst)
            {
                float dist2 = Vector3.Distance(PlayerObject.transform.position, Panel.transform.position);

                if (dist2 < threshold2)
                {
                    TurnHelpPanelOff();
                }
                    
            }


            if (MenuButton.GetStateDown(RightPose.inputSource) && Active)
            {
                HidePad();
                return;
            }

            if (MenuButton.GetStateDown(RightPose.inputSource) && !Active)
            {
                ShowPad();
                return;
            }
        }

        //private void ArrowMove()
        //{
        //    Arrow.localEulerAngles = new Vector3(Arrow.rotation.x, Arrow.rotation.y, MainCam.rotation.y);
        //}



        public void StartCapsule()   //Animation für kapselFlug wird gestartet
        {
            if (!CatchedEmAll)    //Dadurch wird standartprozedur bei zweitem drücken nicht ausgelöst
            {
                CapsuleAnim.SetBool("start", true);
                //PlayerAnim.SetBool("start", true);

                StartCoroutine(TimeUntilLanded());
                StartCoroutine(TimeUntilDoorOpened());
                Invoke("ReadyToLeaveCapsule", 24);
                StartCoroutine(SetTrue());
                Invoke("ChangeHimmelCubemap", 1.0f);

            }
            
        }

        public void ChangeHimmelCubemap()
        {
            RenderSettings.skybox = Himmel;
            RenderSettings.customReflection = HimmelReflection;
        }

        //public void CloseDoor()   //Türe wird Geschlossen
        //{

        //    //AlarmSoundAudio.Play();
        //    DoorAnim.SetBool("open", false);

        //    StartCoroutine(SetTrue()); //Sonst geht türe immer wieder auf und zu du weißt

            


        //}

        IEnumerator SetTrue()
        {


            yield return new WaitForSeconds(4);


            Terrain.SetActive(true);

            yield return new WaitForSeconds(3);

            Hangar.SetActive(false);

            //RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Trilight;  // Skybox wird zu Gradient
            


            yield return new WaitForSeconds(LandTime + DoorOpenTime);
        }



        IEnumerator TimeUntilLanded()
        {
            yield return new WaitForSeconds(LandTime - 2);
            CapsuleLanded();
            //yield return new WaitForSeconds(6);
            //TurnFlashLightOn();
        }

        IEnumerator TimeUntilDoorOpened()
        {
            yield return new WaitForSeconds(LandTime + DoorOpenTime);

            ReadyToLeaveCapsule();
        }

        //public void TurnFlashLightOn()
        //{
        //    Flashlight.SetActive(true);
        //}

        public void CapsuleLanded()
        {
            LandShockWave.SetActive(true);
            Invoke("SetParent", 2.5f);


            Landed = true;
            CapsuleAnim.SetBool("start", false);
            Teleporting.SetActive(true);

            //FindArtefacts.SetActive(true);
            //Panel.SetActive(true);

        }

        public void SetParent()
        {
            PlayerObject.transform.SetParent(null);    // Player ist nicht mehr child von Kapsel und kann  sich somit wieder frei bewegen

        }

        public void ReadyToLeaveCapsule()
        {
            print("jetzt");
            ForestSoundAudio.Play();
            UHUSoundAudio.Play();
            Teleporting.SetActive(true);
            ShowMenuHint(RechteHand);
            PadActive = true;


            //_teleport.ShowTeleportHint();
            //Invoke("TurnHelpDotsOn", 2.0f);
        }

        public void FoundAllItems()
        {
            //CapsuleAnim.SetBool("shouldHover", false);
            CatchedEmAll = true;
            TeleportArea.SetActive(true);
            CompassCapsule.GetComponent<Renderer>().enabled = true;

            ItemScreen.SetActive(false);
            ReturnScreen.SetActive(true);

            //FindArtefacts.SetActive(false);
            //Panel.SetActive(false);

            //ComeBack.SetActive(true);
            //Panel.SetActive(true);
        }


        public void RestartGame()
        {
            SceneManager.LoadScene(0);
        }

        public void BackHome()
        {
            if(CatchedEmAll)
            {

                //damit Player Teil von der Capsule ist und bei der Animation mitfliegt
                PlayerObject.transform.SetParent(PlayerSpawn.transform);


                //Animationen
                CapsuleAnim.SetBool("backhome", true);
                CapsuleAnim.SetBool("start", false);

                //Zeug an und ausschalten
                TeleportArea.SetActive(false);
                Hangar.SetActive(true);
                HelpDots.SetActive(false);
                Teleporting.SetActive(false);
                CompassCapsule.GetComponent<Renderer>().enabled = false;

                Invoke("RestartGame", 27.0f);
                //StartCoroutine(BackHomeDelay());

            }
        }

        //IEnumerator BackHomeDelay()
        //{
        //    yield return new WaitForSeconds(27);

        //    RestartGame();
        //}

        public void TurnHelpPanelOn()
        {
            PanelAnim.SetBool("on", true);
            PanelAnim.SetBool("off", false);
        }

        public void TurnHelpPanelOff()
        {
            ausgelöst = true; ;
            PanelAnim.SetBool("off", true);
            PanelAnim.SetBool("on", false);
            
        }

        public void TurnHelpDotsOn()
        {
            HelpDots.SetActive(true);
            print("isthier");
            print(HelpDots.activeSelf);
        }




        private void ShowPad()
        {
            if (PadActive)
            {
                Pad.SetActive(true);
                PadAnim.SetBool("Open", true);
                Active = true;
                HideMenuHint(RechteHand);
                Invoke("ShowKompasse", 1.0f);
                PadOpenCounter++;
            }

            //print("Hallo");
            //Pose.GetBlendedPose();
            //Pose.GetBlendedPose(RightPose);
            //Pose.GetBlendingBehaviour("Holding");
            //print(MenuButton.GetStateDown(RightPose.inputSource));
            //RightHand.GetComponent<SkinnedMeshRenderer>().enabled = false;

        }
        private void HidePad()
        {
            PadAnim.SetBool("Open", false);
            Invoke("HidePadDelay", 1.5f);

            //print("Du");
            //Pose.GetBlendedPose();


        }

        private void ShowKompasse()
        {
            Kompasse.SetActive(true);

        }

        private void HidePadDelay()
        {
            Pad.SetActive(false);
            Kompasse.SetActive(false);

            //Pose.GetBlendedPose(RightPose);
            Active = false;
            //RightHand.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
            if (PadOpenCounter == 1)
            {
                _teleport.ShowTeleportHint();
            }
        }

        public void ShowMenuHint(Hand hand)
        {
            ControllerButtonHints.ShowButtonHint(hand, MenuAction);
            ControllerButtonHints.ShowTextHint(hand, MenuAction, "Tablet");

        }

        private void HideMenuHint(Hand hand)
        {
            ControllerButtonHints.HideButtonHint(hand, MenuAction);
            ControllerButtonHints.HideTextHint(hand, MenuAction);
        }



        ///////////////////////////////
        ///Artefakte zum zeigen auf pad
        ////////////////



        private void TopfHoloOn()
        {
            TopfHolo.SetActive(true);
        }
        private void PflugfHoloOn()
        {

            PflugHolo.SetActive(true);
        }
        private void KnochenHoloOn()
        {

            KnochenHolo.SetActive(true);
        }
    }





}