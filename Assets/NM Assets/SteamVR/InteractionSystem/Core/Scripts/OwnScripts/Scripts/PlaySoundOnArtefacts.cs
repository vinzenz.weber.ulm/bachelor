﻿using UnityEngine;
using System.Collections;
using System;


namespace Valve.VR.InteractionSystem
{
    public class PlaySoundOnArtefacts : MonoBehaviour
    {
        public GameObject Player;
        public GameObject Artefact;
        public int Threshold;
        public AudioSource Sound;
        public AudioClip Sourcesound;

        public Interactable _Interactable;

        private int count = 0;


        [Header("Animation")]
        public Animator ArtefactAnim;


        void Start()
        {
            Sound = GetComponent<AudioSource>();
            Sound.clip = Sourcesound;

            ArtefactAnim = Artefact.GetComponent<Animator>();
        }
        void Update()
        {

            //print(_Interactable.grabbed);
            float dist = Vector3.Distance(Player.transform.position, Artefact.transform.position);

            //Debug.Log(dist);
            if (dist < Threshold && !_Interactable.grabbed)
            {

                if (count == 0 )
                {
                    PlaySound();
                    ArtefactAnim.SetBool("close", true);

                }
                count++;
            }
            if (dist > Threshold || _Interactable.grabbed)
            {
                count = 0;
                Sound.Stop();
                ArtefactAnim.SetBool("close", false);
            }

            if (_Interactable.grabbed)
            {
                Artefact.GetComponent<PlaySoundOnArtefacts>().enabled = false;
            }

            void PlaySound()
            {
                Sound.Play();
            }
        }
    }
}



